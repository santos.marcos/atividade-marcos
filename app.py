from http.server import BaseHTTPRequestHandler, HTTPServer
import urllib.parse

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(bytes('<html><head><title>Celsius to Fahrenheit</title></head>', 'utf-8'))
        self.wfile.write(bytes('<body><h1>Celsius to Fahrenheit</h1>', 'utf-8'))
        self.wfile.write(bytes('<form method="post">', 'utf-8'))
        self.wfile.write(bytes('Celsius: <input type="text" name="celsius"><br>', 'utf-8'))
        self.wfile.write(bytes('<input type="submit" value="Convert"><br>', 'utf-8'))
        self.wfile.write(bytes('</form>', 'utf-8'))
        
        if self.path.startswith('/convert'):
            qs = urllib.parse.urlparse(self.path).query
            celsius = float(urllib.parse.parse_qs(qs)['celsius'][0])
            fahrenheit = celsius * 9/5 + 32
            self.wfile.write(bytes('<p>Fahrenheit: %.2f</p>' % fahrenheit, 'utf-8'))

        self.wfile.write(bytes('</body></html>', 'utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        celsius = float(urllib.parse.parse_qs(post_data.decode('utf-8'))['celsius'][0])
        fahrenheit = celsius * 9/5 + 32
        self.send_response(301)
        self.send_header('Location', '/convert?celsius=' + str(celsius))
        self.end_headers()

PORT = 8080
httpd = HTTPServer(('localhost', PORT), MyServer)
print('Server running on port %d' % PORT)
httpd.serve_forever()
