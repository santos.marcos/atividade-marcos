FROM python:latest
WORKDIR /app
COPY app.py .
EXPOSE 8080
CMD [ "python", "app.py" ]
